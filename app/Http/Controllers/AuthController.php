<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome2(){
        return view('halaman.welcome2');
    }

    public function kirim(Request $request){
        $nama = $request['nama'];
        $alamat = $request['address'];
        return view('halaman.home', compact('nama','alamat'));
    }

}


